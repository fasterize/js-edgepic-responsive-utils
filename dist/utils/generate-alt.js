"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.generateAlt = void 0;
/**
 * Genrate alt from image src
 * The generated alt is relative to the image name
 *
 * @param {String} src - image src
 * @return {String} - The generated alt
 */

var generateAlt = function generateAlt(src) {
  if (!src) return;
  var removeConnectorsRegex = /\+|-|_|&/g;
  var parts = src.split('/');
  var imageName = (parts[parts.length - 1] || '').replace(removeConnectorsRegex, ' ').toLocaleLowerCase();
  var extensionIndex = imageName.indexOf('.');
  return imageName.slice(0, extensionIndex);
};
exports.generateAlt = generateAlt;