"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.isSupportedInBrowser = void 0;
var _isServer = require("./is-server");
var isSupportedInBrowser = function isSupportedInBrowser(isBlurHash) {
  var support = true;
  if (isBlurHash && !(0, _isServer.isServer)()) {
    try {
      new window.ImageData(new Uint8ClampedArray([0, 0, 0, 0]), 1, 1);
    } catch (e) {
      support = false;
    }
  }
  return Element.prototype.hasOwnProperty('prepend') && support;
};
exports.isSupportedInBrowser = isSupportedInBrowser;