"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getPreviewSRC = void 0;
var _generateUrl = require("../utils/generate-url");
var _excluded = ["width", "height", "sizes"];
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }
function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }
function _defineProperty(obj, key, value) { key = _toPropertyKey(key); if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
function _toPropertyKey(arg) { var key = _toPrimitive(arg, "string"); return _typeof(key) === "symbol" ? key : String(key); }
function _toPrimitive(input, hint) { if (_typeof(input) !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (_typeof(res) !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }
function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }
function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }
var getPreviewSRC = function getPreviewSRC(_ref) {
  var _ref$config = _ref.config,
    config = _ref$config === void 0 ? {} : _ref$config,
    _containerProps = _ref.containerProps,
    params = _ref.params,
    src = _ref.src,
    devicePixelRatio = _ref.devicePixelRatio;
  var width = _containerProps.width,
    height = _containerProps.height,
    sizes = _containerProps.sizes,
    restContainerProps = _objectWithoutProperties(_containerProps, _excluded);
  var previewQualityFactor = config.previewQualityFactor;
  var lowQualitySize = getLowQualitySize({
    width: width,
    height: height
  }, previewQualityFactor);
  return (0, _generateUrl.generateURL)({
    src: src,
    config: config,
    containerProps: restContainerProps,
    params: _objectSpread(_objectSpread({}, params), lowQualitySize),
    devicePixelRatio: devicePixelRatio
  });
};
exports.getPreviewSRC = getPreviewSRC;
var getLowQualitySize = function getLowQualitySize() {
  var params = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var factor = arguments.length > 1 ? arguments[1] : undefined;
  var width = params.width,
    height = params.height;
  width = width ? Math.floor(width / factor) : null;
  height = height ? Math.floor(height / factor) : null;
  return {
    width: width,
    w: width,
    height: height,
    h: height
  };
};