"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.generateURL = void 0;
var _constants = require("../constants");
var _excluded = ["w", "h", "width", "height"];
function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }
function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }
function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }
function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }
function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }
function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i]; return arr2; }
function _iterableToArrayLimit(arr, i) { var _i = null == arr ? null : "undefined" != typeof Symbol && arr[Symbol.iterator] || arr["@@iterator"]; if (null != _i) { var _s, _e, _x, _r, _arr = [], _n = !0, _d = !1; try { if (_x = (_i = _i.call(arr)).next, 0 === i) { if (Object(_i) !== _i) return; _n = !1; } else for (; !(_n = (_s = _x.call(_i)).done) && (_arr.push(_s.value), _arr.length !== i); _n = !0); } catch (err) { _d = !0, _e = err; } finally { try { if (!_n && null != _i.return && (_r = _i.return(), Object(_r) !== _r)) return; } finally { if (_d) throw _e; } } return _arr; } }
function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }
function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }
function _defineProperty(obj, key, value) { key = _toPropertyKey(key); if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
function _toPropertyKey(arg) { var key = _toPrimitive(arg, "string"); return _typeof(key) === "symbol" ? key : String(key); }
function _toPrimitive(input, hint) { if (_typeof(input) !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (_typeof(res) !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }
var generateURL = function generateURL(props) {
  var src = props.src,
    params = props.params,
    _props$config = props.config,
    config = _props$config === void 0 ? {} : _props$config,
    _props$containerProps = props.containerProps,
    containerProps = _props$containerProps === void 0 ? {} : _props$containerProps,
    _props$devicePixelRat = props.devicePixelRatio,
    devicePixelRatio = _props$devicePixelRat === void 0 ? 1 : _props$devicePixelRat,
    processURL = props.processURL,
    processQueryString = props.processQueryString,
    service = props.service;
  var sizes = containerProps.sizes,
    doNotReplaceImageUrl = containerProps.doNotReplaceImageUrl;
  var size = sizes && sizes[_constants.DEVICE_PIXEL_RATIO_LIST.indexOf(devicePixelRatio)];
  var _ref = size || {},
    width = _ref.width,
    height = _ref.height;
  var token = config.token,
    domain = config.domain,
    doNotReplaceURL = config.doNotReplaceURL,
    customDomain = config.customDomain,
    apiVersion = config.apiVersion;
  var finalDomain = customDomain ? domain : token + '.' + domain;
  var finalApiVersion = apiVersion ? apiVersion + '/' : '';
  var url = [doNotReplaceURL || doNotReplaceImageUrl ? '' : "https://".concat(finalDomain, "/").concat(finalApiVersion), src, src.indexOf('?') !== -1 ? '&' : '?'].join('');
  return [processURL ? processURL({
    url: url,
    token: token,
    domain: domain,
    service: service
  }) : url, getQueryString({
    params: _objectSpread(_objectSpread({}, config.params || {}), params),
    width: width,
    height: height,
    config: config,
    processQueryString: processQueryString,
    devicePixelRatio: devicePixelRatio,
    service: service
  })].join('');
};
exports.generateURL = generateURL;
var getQueryString = function getQueryString(props) {
  var _props$params = props.params,
    params = _props$params === void 0 ? {} : _props$params,
    width = props.width,
    height = props.height,
    _props$config2 = props.config,
    config = _props$config2 === void 0 ? {} : _props$config2,
    processQueryString = props.processQueryString,
    devicePixelRatio = props.devicePixelRatio,
    service = props.service;
  var processOnlyWidth = config.processOnlyWidth;
  var _processParamsExceptS = processParamsExceptSizeRelated(params),
    _processParamsExceptS2 = _slicedToArray(_processParamsExceptS, 3),
    restParams = _processParamsExceptS2[0],
    _processParamsExceptS3 = _processParamsExceptS2[1],
    widthFromParam = _processParamsExceptS3 === void 0 ? null : _processParamsExceptS3,
    heightFromParam = _processParamsExceptS2[2];
  var widthQ = width ? width : widthFromParam;
  var heightQ = height ? height : heightFromParam;
  var restParamsQ = Object.keys(restParams).map(function (key) {
    return encodeURIComponent(key) + "=" + encodeURIComponent(restParams[key]);
  }).join('&');
  var query = [widthQ ? "w=".concat(widthQ) : '', heightQ && !processOnlyWidth ? (widthQ ? '&' : '') + "h=".concat(heightQ) : '', restParamsQ ? '&' + restParamsQ : ''].join('');
  return processQueryString ? processQueryString({
    query: query,
    widthQ: widthQ,
    heightQ: heightQ,
    restParamsQ: restParamsQ,
    processOnlyWidth: processOnlyWidth,
    devicePixelRatio: devicePixelRatio,
    service: service
  }) : query;
};
var processParamsExceptSizeRelated = function processParamsExceptSizeRelated(params) {
  var w = params.w,
    h = params.h,
    width = params.width,
    height = params.height,
    restParams = _objectWithoutProperties(params, _excluded);
  return [restParams, w || width, h || height];
};