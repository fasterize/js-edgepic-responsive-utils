"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getParentContainerSize = void 0;
var getParentContainerSize = function getParentContainerSize(img) {
  var type = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'width';
  var parentNode = img;
  var size = 0;
  var maxCount = 0;

  // console.log('------------START-----------')
  do {
    var _parentNode, _parentNode2;
    parentNode = (_parentNode2 = parentNode) === null || _parentNode2 === void 0 ? void 0 : _parentNode2.parentNode;
    size = type === 'height' ? parentNode.clientHeight : parentNode.clientWidth;
    if (size) {
      size = typeof parentNode.getBoundingClientRect === 'function' ? parentNode.getBoundingClientRect()[type] : window.innerWidth;
    }
    // console.log('----------', maxCount)
    // console.log('img.NodeName',img.nodeName, img.alt )
    // console.log('size',size )
    // console.log('arguments', arguments)
    // console.log('maxCount',maxCount )
    // console.log('parentNode',parentNode )
    // console.log('parentNode.nodeName + nodeValue',parentNode.nodeName, parent.nodeValue )
    // console.log('bouding',typeof parentNode.getBoundingClientRect === 'function' ? parentNode.getBoundingClientRect() : null, window.innerWidth, type )
    // console.log('clientWidth',parentNode.clientWidth )
    // console.log('clientHeight',parentNode.clientHeight )
    // console.log('----------', maxCount)
    maxCount = maxCount + 1;
  } while ((_parentNode = parentNode) !== null && _parentNode !== void 0 && _parentNode.parentNode && !size && maxCount < 5);

  // console.log('while size', size);

  var leftPadding = size && parentNode && parentNode.nodeType === 1 ? parseInt(window.getComputedStyle(parentNode).paddingLeft) : 0;
  var rightPadding = size && parentNode && parentNode.nodeType === 1 ? parseInt(window.getComputedStyle(parentNode).paddingRight) : 0;
  if (!size) {
    size = window.innerWidth;
  }

  // console.log('padding', leftPadding, rightPadding);
  // console.log('final size', size);
  // console.log('------------END-----------');

  return size + (size ? -leftPadding - rightPadding : 0);
};
exports.getParentContainerSize = getParentContainerSize;