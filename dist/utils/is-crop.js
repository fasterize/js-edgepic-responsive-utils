"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.isCrop = void 0;
var isCrop = function isCrop(func) {
  return ['crop', 'fit', 'bound', 'cover'].indexOf(func) !== -1;
};
exports.isCrop = isCrop;