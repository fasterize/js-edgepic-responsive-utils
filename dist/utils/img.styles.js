"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.previewWrapper = exports.previewImg = exports.picture = exports.img = exports.image = void 0;
function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }
function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }
function _defineProperty(obj, key, value) { key = _toPropertyKey(key); if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
function _toPropertyKey(arg) { var key = _toPrimitive(arg, "string"); return _typeof(key) === "symbol" ? key : String(key); }
function _toPrimitive(input, hint) { if (_typeof(input) !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (_typeof(res) !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }
var picture = function picture(_ref) {
  var preserveSize = _ref.preserveSize,
    imgNodeWidth = _ref.imgNodeWidth,
    imgNodeHeight = _ref.imgNodeHeight,
    ratio = _ref.ratio,
    previewLoaded = _ref.previewLoaded,
    loaded = _ref.loaded,
    placeholderBackground = _ref.placeholderBackground,
    operation = _ref.operation;
  return _objectSpread(_objectSpread({
    width: getPictureWidth({
      operation: operation,
      preserveSize: preserveSize,
      imgNodeWidth: imgNodeWidth
    }),
    height: getPictureHeight({
      operation: operation,
      preserveSize: preserveSize,
      imgNodeHeight: imgNodeHeight
    }),
    position: 'relative',
    background: !previewLoaded && !loaded && operation !== 'bound' ? placeholderBackground : 'transparent'
  }, operation === 'bound' && {
    display: 'inline-block'
  }), {}, {
    transform: 'translateZ(0)'
  }, ratio && operation !== 'bound' && {
    paddingBottom: 100 / ratio + '%',
    overflow: 'hidden'
  });
};
exports.picture = picture;
var image = function image(_ref2) {
  var preserveSize = _ref2.preserveSize,
    operation = _ref2.operation,
    imgNodeWidth = _ref2.imgNodeWidth,
    imgNodeHeight = _ref2.imgNodeHeight;
  return {
    width: getPictureWidth({
      operation: operation,
      preserveSize: preserveSize,
      imgNodeWidth: imgNodeWidth
    }),
    height: getPictureHeight({
      operation: operation,
      preserveSize: preserveSize,
      imgNodeHeight: imgNodeHeight
    })
  };
};
exports.image = image;
var getPictureWidth = function getPictureWidth(_ref3) {
  var operation = _ref3.operation,
    preserveSize = _ref3.preserveSize,
    imgNodeWidth = _ref3.imgNodeWidth;
  if (preserveSize && imgNodeWidth) {
    return imgNodeWidth;
  }
  switch (operation) {
    case 'bound':
      {
        return 'auto';
      }
    default:
      return '100%';
  }
};
var getPictureHeight = function getPictureHeight(_ref4) {
  var operation = _ref4.operation,
    preserveSize = _ref4.preserveSize,
    imgNodeHeight = _ref4.imgNodeHeight;
  if (preserveSize && imgNodeHeight) {
    return imgNodeHeight;
  }
  switch (operation) {
    default:
      return 'auto';
  }
};
var previewWrapper = function previewWrapper() {
  return {
    transform: 'translateZ(0)',
    zIndex: '1',
    height: '100%',
    width: '100%',
    position: 'absolute',
    top: '0',
    left: '0',
    overflow: 'hidden'
  };
};
exports.previewWrapper = previewWrapper;
var previewImg = function previewImg(_ref5) {
  var loaded = _ref5.loaded;
  return _objectSpread({
    opacity: loaded ? 0 : 1,
    height: '100%',
    width: '100%'
  }, animation(true));
};
exports.previewImg = previewImg;
var img = function img(_ref6) {
  var isPreview = _ref6.isPreview,
    loaded = _ref6.loaded,
    operation = _ref6.operation;
  return _objectSpread(_objectSpread({
    display: 'block',
    width: getImgWidth({
      operation: operation
    })
  }, getImgPosition({
    operation: operation
  })), {}, {
    opacity: loaded ? 1 : 0
  }, isPreview ? {} : animation(!loaded));
};
exports.img = img;
var getImgWidth = function getImgWidth(_ref7) {
  var operation = _ref7.operation;
  switch (operation) {
    case 'bound':
      {
        return 'auto';
      }
    default:
      {
        return '100%';
      }
  }
};
var getImgPosition = function getImgPosition(_ref8) {
  var operation = _ref8.operation;
  switch (operation) {
    case 'bound':
      {
        return {
          position: 'relative'
        };
      }
    default:
      {
        return {
          position: 'absolute',
          top: 0,
          left: 0
        };
      }
  }
};
var animation = function animation(isON) {
  return {
    transform: isON ? "scale(1.1)" : 'scale(1)',
    filter: isON ? "blur(10px)" : 'blur(0)'
  };
};