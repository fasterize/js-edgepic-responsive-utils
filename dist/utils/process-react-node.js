"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.processReactNode = void 0;
var _generateUrl = require("../utils/generate-url");
var _getBreakpoint = require("../utils/get-breakpoint");
var _getImgSrc = require("../utils/get-img-src");
var _getPreviewSrc = require("../utils/get-preview-src");
var _isLowQualityPreview = require("../utils/is-low-quality-preview");
var _processParams = require("../utils/process-params");
var _determineContainerProps = require("../utils/determine-container-props");
var _isServer = require("./is-server");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }
function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }
function _defineProperty(obj, key, value) { key = _toPropertyKey(key); if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
function _toPropertyKey(arg) { var key = _toPrimitive(arg, "string"); return _typeof(key) === "symbol" ? key : String(key); }
function _toPrimitive(input, hint) { if (_typeof(input) !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (_typeof(res) !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }
function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }
function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }
function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i]; return arr2; }
function _iterableToArrayLimit(arr, i) { var _i = null == arr ? null : "undefined" != typeof Symbol && arr[Symbol.iterator] || arr["@@iterator"]; if (null != _i) { var _s, _e, _x, _r, _arr = [], _n = !0, _d = !1; try { if (_x = (_i = _i.call(arr)).next, 0 === i) { if (Object(_i) !== _i) return; _n = !1; } else for (; !(_n = (_s = _x.call(_i)).done) && (_arr.push(_s.value), _arr.length !== i); _n = !0); } catch (err) { _d = !0, _e = err; } finally { try { if (!_n && null != _i.return && (_r = _i.return(), Object(_r) !== _r)) return; } finally { if (_d) throw _e; } } return _arr; } }
function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }
var processReactNode = function processReactNode(props, imgNode, isUpdate, windowScreenBecomesBigger) {
  var lowQualityPreview = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : true;
  var imgProps = getProps(props);
  var imgNodeSRC = imgProps.imgNodeSRC,
    params = imgProps.params,
    sizes = imgProps.sizes,
    adaptive = imgProps.adaptive;
  var _props$config = props.config,
    config = _props$config === void 0 ? {} : _props$config;
  var baseURL = config.baseURL,
    presets = config.presets,
    minLowQualityWidth = config.minLowQualityWidth,
    devicePixelRatioList = config.devicePixelRatioList,
    _config$params = config.params,
    configParams = _config$params === void 0 ? {} : _config$params;
  if (!imgNodeSRC) return;
  var _getImgSRC = (0, _getImgSrc.getImgSRC)(imgNodeSRC, baseURL),
    _getImgSRC2 = _slicedToArray(_getImgSRC, 2),
    src = _getImgSRC2[0],
    svg = _getImgSRC2[1];
  var previewCloudimgURL, size;
  if (adaptive) {
    size = (0, _getBreakpoint.getBreakpoint)(sizes, presets);
    if (size) {
      if (size.params.src) {
        var _getImgSRC3 = (0, _getImgSrc.getImgSRC)(size.params.src, baseURL);
        var _getImgSRC4 = _slicedToArray(_getImgSRC3, 2);
        src = _getImgSRC4[0];
        svg = _getImgSRC4[1];
      }
    }
  } else {
    if (isUpdate && !windowScreenBecomesBigger) return;
  }
  var containerProps = (0, _determineContainerProps.determineContainerProps)(_objectSpread({
    imgNode: imgNode,
    config: config,
    size: size
  }, imgProps));
  var width = containerProps.width;
  var preview = !(0, _isServer.isServer)() ? lowQualityPreview && (0, _isLowQualityPreview.isLowQualityPreview)(adaptive, width, svg, minLowQualityWidth) : null;
  var generateURLbyDPR = function generateURLbyDPR(devicePixelRatio) {
    return !adaptive && svg ? src : (0, _generateUrl.generateURL)({
      src: src,
      params: params,
      config: config,
      containerProps: containerProps,
      devicePixelRatio: devicePixelRatio
    });
  };
  var cloudimgURL = !(0, _isServer.isServer)() ? generateURLbyDPR(Number(window.devicePixelRatio.toFixed(1))) : null;
  var cloudimgSRCSET = !(0, _isServer.isServer)() ? devicePixelRatioList.map(function (dpr) {
    return {
      dpr: dpr.toString(),
      url: generateURLbyDPR(dpr)
    };
  }) : null;
  var operation = params.func || configParams.func;
  if (preview) {
    previewCloudimgURL = (0, _getPreviewSrc.getPreviewSRC)({
      src: src,
      params: params,
      config: config,
      containerProps: containerProps
    });
  }
  return _objectSpread({
    imgProps: imgProps,
    cloudimgURL: cloudimgURL,
    previewCloudimgURL: previewCloudimgURL,
    cloudimgSRCSET: cloudimgSRCSET,
    processed: true,
    preview: preview,
    operation: operation
  }, containerProps);
};
exports.processReactNode = processReactNode;
var getProps = function getProps(_ref) {
  var src = _ref.src,
    width = _ref.width,
    height = _ref.height,
    ratio = _ref.ratio,
    params = _ref.params,
    sizes = _ref.sizes,
    doNotReplaceURL = _ref.doNotReplaceURL;
  return {
    imgNodeSRC: src || '',
    imgNodeWidth: width || null,
    imgNodeHeight: height || null,
    imgNodeRatio: ratio,
    params: (0, _processParams.processParams)(params),
    sizes: sizes,
    adaptive: !!sizes,
    doNotReplaceImageUrl: doNotReplaceURL || false
  };
};