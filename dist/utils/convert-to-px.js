"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.convertToPX = void 0;
var _isServer = require("./is-server");
var convertToPX = function convertToPX() {
  var size = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
  size = (size || '').toString();
  if (size.indexOf('px') > -1) {
    return parseInt(size);
  } else if (size.indexOf('%') > -1) {
    // todo calculate container width * %
    // todo could be the potenial problem when the plugin set 100% width and on update it can calculate wrong value
    return null;
  } else if (size.indexOf('vw') > -1) {
    return !(0, _isServer.isServer)() ? window.innerWidth * parseInt(size) / 100 : null;
  } else if (size.indexOf('vh') > -1) {
    return !(0, _isServer.isServer)() ? window.innerHeight * parseInt(size) / 100 : null;
  }
  return parseInt(size) || '';
};
exports.convertToPX = convertToPX;