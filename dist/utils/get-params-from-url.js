"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getParamsFromURL = void 0;
var getParamsFromURL = function getParamsFromURL(url) {
  var queryIndex = url.indexOf('?');
  if (queryIndex === -1) return;
  return processParams(url.slice(queryIndex + 1));
};
exports.getParamsFromURL = getParamsFromURL;
var processParams = function processParams(params) {
  var resultParams = undefined;
  try {
    var temp = params.replace(/(\w+:)|(\w+ :)/g, function (matchedStr) {
      return '"' + matchedStr.substring(0, matchedStr.length - 1) + '":';
    });
    resultParams = JSON.parse(temp);
  } catch (e) {}
  if (!resultParams) {
    try {
      resultParams = JSON.parse('{"' + decodeURI(params.replace(/&/g, "\",\"").replace(/=/g, "\":\"")) + '"}');
    } catch (e) {}
  }
  return resultParams;
};