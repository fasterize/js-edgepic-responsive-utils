"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getRatio = void 0;
var getRatio = function getRatio(_ref) {
  var imgNodeRatio = _ref.imgNodeRatio,
    width = _ref.width,
    height = _ref.height,
    size = _ref.size,
    _ref$config = _ref.config,
    config = _ref$config === void 0 ? {} : _ref$config,
    imgNodeWidth = _ref.imgNodeWidth,
    imgNodeHeight = _ref.imgNodeHeight;
  var imageSizeAttributes = config.imageSizeAttributes;
  var ignoreNodeRatio = imageSizeAttributes === 'ignore';
  if (size && size.params) {
    if (size.params.r || size.params.ratio) {
      return size.params.r || size.params.ratio;
    } else if ((size.params.w || size.params.width) && (size.params.h || size.params.height)) {
      return (size.params.w || size.params.width) / (size.params.h || size.params.height);
    } else {
      return null;
    }
  }
  if (!ignoreNodeRatio && imgNodeWidth && imgNodeHeight) {
    return imgNodeWidth / imgNodeHeight;
  } else if (!ignoreNodeRatio && imgNodeRatio) {
    return imgNodeRatio;
  } else if (width && height) {
    return width / height;
  }
  return null;
};
exports.getRatio = getRatio;