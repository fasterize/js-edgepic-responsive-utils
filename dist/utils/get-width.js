"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getWidth = void 0;
var _convertToPx = require("../utils/convert-to-px");
var _getParentContainerSize = require("../utils/get-parent-container-size");
var _isServer = require("./is-server");
/**
 * Get width for an image.
 *
 * Priority:
 * 1. image node param width
 * 2. image node image width
 * 3. image node inline styling
 * 4. parent node of image computed style width (up to body tag)
 *
 * @param {HTMLImageElement} props.imgNode - image node
 * @param {Number} props.imgNodeWidth - width of image node
 * @param {String} props.params - params of image node
 * @return {Array} [with, isLimit]
 */
var getWidth = function getWidth(props) {
  var imgNode = props.imgNode,
    _props$imgNodeWidth = props.imgNodeWidth,
    imgNodeWidth = _props$imgNodeWidth === void 0 ? null : _props$imgNodeWidth,
    _props$params = props.params,
    params = _props$params === void 0 ? {} : _props$params,
    size = props.size,
    _props$config = props.config,
    config = _props$config === void 0 ? {} : _props$config;
  var _ignoreNodeImgSize = config.ignoreNodeImgSize,
    ignoreStyleImgSize = config.ignoreStyleImgSize,
    imageSizeAttributes = config.imageSizeAttributes,
    detectImageNodeCSS = config.detectImageNodeCSS;
  var ignoreNodeImgSize = typeof _ignoreNodeImgSize !== 'undefined' ? _ignoreNodeImgSize : imageSizeAttributes !== 'use';
  var sizeParamsWidth = size && size.params && (size.params.w || size.params.width);
  var paramsWidth = params.width || params.w;
  var imgNodeWidthPX = !ignoreNodeImgSize && imgNodeWidth && (0, _convertToPx.convertToPX)(imgNodeWidth);
  var imageWidth = !ignoreStyleImgSize && getImageWidth(imgNode, detectImageNodeCSS);
  var imageContainerWidth = !(0, _isServer.isServer)() ? !imageWidth && parseInt((0, _getParentContainerSize.getParentContainerSize)(imgNode), 10) : null;
  var resultWidth = imageWidth || imageContainerWidth;
  if (size && size.params) {
    if (size.params.r) {
      if (params.width || params.w) {
        return [paramsWidth];
      }
      if (!ignoreNodeImgSize && imgNodeWidth) {
        return [imgNodeWidthPX];
      }
      return [resultWidth];
    }
    return [sizeParamsWidth];
  }
  if (paramsWidth) {
    return [paramsWidth];
  }
  if (!ignoreNodeImgSize && imgNodeWidth) {
    return [imgNodeWidthPX];
  }
  return [resultWidth, true];
};

/**
 * Get width for an image.
 *
 *
 * @param {HTMLImageElement} img - image node
 * @param {Boolean} detectImageNodeCSS - detect width of image node
 * @return {Number} width of image container
 */
exports.getWidth = getWidth;
var getImageWidth = function getImageWidth(img, detectImageNodeCSS) {
  var isImageStyleWidthInPX = img && img.style && img.style.width && !(img.style.width.indexOf('%') !== -1);
  var imageStyleWidth = isImageStyleWidthInPX && img.style.width;
  var imageWidth = imageStyleWidth && (0, _convertToPx.convertToPX)(imageStyleWidth);
  var imageCSSWidth = !(0, _isServer.isServer)() ? detectImageNodeCSS && getImageNodeCSS(img) : false;
  return detectImageNodeCSS && imageCSSWidth ? imageCSSWidth : imageWidth && parseInt(imageWidth, 10);
};
var getImageNodeCSS = function getImageNodeCSS(img) {
  var width;
  var preDisplayValue = img.style.display;
  img.style.display = 'inline-block';
  width = img.getBoundingClientRect().width;
  img.style.display = preDisplayValue;
  return width;
};