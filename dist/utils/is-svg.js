"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.isSVG = void 0;
var isSVG = function isSVG(url) {
  return url.slice(-4).toLowerCase() === '.svg';
};
exports.isSVG = isSVG;