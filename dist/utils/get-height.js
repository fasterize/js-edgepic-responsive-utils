"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getImageHeight = exports.getHeight = void 0;
var _convertToPx = require("../utils/convert-to-px");
var _getParentContainerSize = require("../utils/get-parent-container-size");
var _isCrop = require("../utils/is-crop");
var _isServer = require("./is-server");
/**
 * Get height for an image.
 *
 * Priority:
 * 1. image node param height
 * 2. image node image height
 * 3. image node inline styling
 * 4. parent node of image computed style height (up to body tag)
 *
 * @param {HTMLImageElement} props.imgNode - image node
 * @param {Object} props.config - plugin config
 * @param {Boolean} props.exactSize - a flag to use exact width/height params
 * @param {Number} props.imgNodeHeight - height of image node
 * @param {String} props.params - params of image node
 * @return {Number} height limit
 */
var getHeight = function getHeight(props) {
  var _props$imgNode = props.imgNode,
    imgNode = _props$imgNode === void 0 ? null : _props$imgNode,
    _props$config = props.config,
    config = _props$config === void 0 ? {} : _props$config,
    _props$imgNodeHeight = props.imgNodeHeight,
    imgNodeHeight = _props$imgNodeHeight === void 0 ? null : _props$imgNodeHeight,
    _props$params = props.params,
    params = _props$params === void 0 ? {} : _props$params,
    size = props.size,
    width = props.width;
  var _ignoreNodeImgSize = config.ignoreNodeImgSize,
    ignoreStyleImgSize = config.ignoreStyleImgSize,
    imageSizeAttributes = config.imageSizeAttributes,
    _config$params = config.params,
    configParams = _config$params === void 0 ? {} : _config$params;
  var ignoreNodeImgSize = typeof _ignoreNodeImgSize !== 'undefined' ? _ignoreNodeImgSize : imageSizeAttributes !== 'use';
  var crop = (0, _isCrop.isCrop)(params.func || configParams.func);
  var sizeParamsHeight = size && size.params && (size.params.h || size.params.height);
  var paramsRatio = size && size.params && (size.params.ratio || size.params.r);
  var paramsHeight = params.height || params.h;
  var imgNodeHeightPX = !ignoreNodeImgSize && imgNodeHeight && (0, _convertToPx.convertToPX)(imgNodeHeight);
  var imageHeight = !ignoreStyleImgSize && getImageHeight(imgNode);
  var imageContainerHeight = !(0, _isServer.isServer)() ? !imageHeight && parseInt((0, _getParentContainerSize.getParentContainerSize)(imgNode, 'height'), 10) : null;
  if (size && size.params) {
    if (paramsRatio && width) {
      return width / paramsRatio;
    }
    return sizeParamsHeight;
  }
  if (paramsHeight) {
    return paramsHeight;
  }
  if (!ignoreNodeImgSize && imgNodeHeight) {
    return imgNodeHeightPX;
  }
  if (imageHeight) {
    return imageHeight;
  }
  if (!crop) {
    return null;
  }
  return imageContainerHeight;
};

/**
 * Get height for an image.
 *
 *
 * @param {HTMLImageElement} img - image node
 * @return {Number|null} height of image container
 */
exports.getHeight = getHeight;
var getImageHeight = function getImageHeight(img) {
  var imageHeight;
  var imageStyleHeight = img && img.style && img.style.height;
  var isImageStyleHeightInPX = imageStyleHeight && !(imageStyleHeight.indexOf('%') !== -1);
  imageStyleHeight = isImageStyleHeightInPX && imageStyleHeight || '';
  imageHeight = (0, _convertToPx.convertToPX)(imageStyleHeight);
  return imageHeight && parseInt(imageHeight, 10);
};
exports.getImageHeight = getImageHeight;