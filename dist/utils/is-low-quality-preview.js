"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.isLowQualityPreview = void 0;
var isLowQualityPreview = function isLowQualityPreview(adaptive, width, svg, minLowQualityWidth) {
  return adaptive ? width > minLowQualityWidth : width > minLowQualityWidth && !svg;
};
exports.isLowQualityPreview = isLowQualityPreview;