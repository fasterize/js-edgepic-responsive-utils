"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.determineContainerProps = void 0;
var _isCrop = require("./is-crop");
var _getWidth3 = require("./get-width");
var _getHeight = require("./get-height");
var _getRatio = require("./get-ratio");
var _constants = require("../constants");
var _getSizeLimit = require("./get-size-limit");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }
function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }
function _defineProperty(obj, key, value) { key = _toPropertyKey(key); if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
function _toPropertyKey(arg) { var key = _toPrimitive(arg, "string"); return _typeof(key) === "symbol" ? key : String(key); }
function _toPrimitive(input, hint) { if (_typeof(input) !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (_typeof(res) !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }
function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }
function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }
function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i]; return arr2; }
function _iterableToArrayLimit(arr, i) { var _i = null == arr ? null : "undefined" != typeof Symbol && arr[Symbol.iterator] || arr["@@iterator"]; if (null != _i) { var _s, _e, _x, _r, _arr = [], _n = !0, _d = !1; try { if (_x = (_i = _i.call(arr)).next, 0 === i) { if (Object(_i) !== _i) return; _n = !1; } else for (; !(_n = (_s = _x.call(_i)).done) && (_arr.push(_s.value), _arr.length !== i); _n = !0); } catch (err) { _d = !0, _e = err; } finally { try { if (!_n && null != _i.return && (_r = _i.return(), Object(_r) !== _r)) return; } finally { if (_d) throw _e; } } return _arr; } }
function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }
var determineContainerProps = function determineContainerProps(props) {
  var imgNode = props.imgNode,
    _props$config = props.config,
    config = _props$config === void 0 ? {} : _props$config,
    imgNodeWidth = props.imgNodeWidth,
    imgNodeHeight = props.imgNodeHeight,
    imgNodeRatio = props.imgNodeRatio,
    params = props.params,
    size = props.size,
    doNotReplaceImageUrl = props.doNotReplaceImageUrl;
  var exactSize = config.exactSize,
    limitFactor = config.limitFactor,
    _config$params = config.params,
    configParams = _config$params === void 0 ? {} : _config$params;
  var ratio = null;
  var crop = (0, _isCrop.isCrop)(params.func || configParams.func);
  var _getWidth = (0, _getWidth3.getWidth)({
      imgNode: imgNode,
      config: config,
      exactSize: exactSize,
      imgNodeWidth: imgNodeWidth,
      params: _objectSpread(_objectSpread({}, configParams), params),
      size: size
    }),
    _getWidth2 = _slicedToArray(_getWidth, 2),
    width = _getWidth2[0],
    isLimit = _getWidth2[1];
  var height = (0, _getHeight.getHeight)({
    imgNode: imgNode,
    config: config,
    exactSize: exactSize,
    imgNodeHeight: imgNodeHeight,
    imgNodeWidth: imgNodeWidth,
    imgNodeRatio: imgNodeRatio,
    params: _objectSpread(_objectSpread({}, configParams), params),
    size: size,
    width: width
  });
  ratio = (0, _getRatio.getRatio)({
    imgNodeRatio: imgNodeRatio,
    width: width,
    height: height,
    size: size,
    config: config,
    imgNodeWidth: imgNodeWidth,
    imgNodeHeight: imgNodeHeight
  });
  var sizes = _constants.DEVICE_PIXEL_RATIO_LIST.map(function (dpr) {
    var widthWithDPR, heightWithDRP;
    widthWithDPR = width && width * dpr;
    widthWithDPR = crop ? widthWithDPR : isLimit ? (0, _getSizeLimit.getSizeLimit)(widthWithDPR, exactSize, limitFactor) : widthWithDPR;
    heightWithDRP = height && height * dpr;
    if (!heightWithDRP && widthWithDPR && ratio) {
      heightWithDRP = Math.floor(widthWithDPR / ratio);
    }
    if (!widthWithDPR && heightWithDRP && ratio) {
      widthWithDPR = Math.floor(heightWithDRP * ratio);
    }
    return {
      width: widthWithDPR,
      height: heightWithDRP,
      ratio: ratio
    };
  });
  return {
    sizes: sizes,
    ratio: ratio,
    width: width,
    height: height,
    doNotReplaceImageUrl: doNotReplaceImageUrl
  };
};
exports.determineContainerProps = determineContainerProps;