"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getBreakpoint = void 0;
var _isServer = require("./is-server");
function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }
function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }
function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }
function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }
function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }
function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i]; return arr2; }
var getBreakpoint = function getBreakpoint(sizes, presets) {
  var size = !(0, _isServer.isServer)() ? getAdaptiveSize(sizes, presets) : [];
  return _toConsumableArray(size).reverse().find(function (item) {
    return window.matchMedia(item.media).matches;
  });
};
exports.getBreakpoint = getBreakpoint;
var getAdaptiveSize = function getAdaptiveSize(sizes, presets) {
  var resultSizes = [];
  Object.keys(sizes).forEach(function (key) {
    var customMedia = key.indexOf(':') > -1;
    var media = customMedia ? key : presets[key];
    resultSizes.push({
      media: media,
      params: normalizeSize(sizes[key])
    });
  });
  return resultSizes;
};
var normalizeSize = function normalizeSize() {
  var params = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var _params$w = params.w,
    w = _params$w === void 0 ? params.width || '' : _params$w,
    _params$h = params.h,
    h = _params$h === void 0 ? params.height || '' : _params$h,
    _params$r = params.r,
    r = _params$r === void 0 ? params.r : _params$r,
    _params$src = params.src,
    src = _params$src === void 0 ? params.src : _params$src;
  if (w.toString().indexOf('vw') > -1) {
    var percent = parseFloat(w);
    w = window.innerWidth * percent / 100;
  } else {
    w = parseFloat(w);
  }
  if (h.toString().indexOf('vh') > -1) {
    var _percent = parseFloat(h);
    h = window.innerHeight * _percent / 100;
  } else {
    h = parseFloat(h);
  }
  return {
    w: w,
    h: h,
    r: r,
    src: src
  };
};