"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.processParams = void 0;
function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }
var processParams = function processParams(params) {
  var resultParams = {};
  if (_typeof(params) === 'object') {
    return params;
  }
  try {
    resultParams = JSON.parse('{"' + decodeURI(params.replace(/&/g, "\",\"").replace(/=/g, "\":\"")) + '"}');
  } catch (e) {}
  if (!resultParams) {
    resultParams = params;
  }
  return resultParams;
};
exports.processParams = processParams;