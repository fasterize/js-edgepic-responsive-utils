"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getImgSRC = void 0;
var _isSvg = require("../utils/is-svg");
var _isServer = require("./is-server");
var getImgSRC = function getImgSRC(src) {
  var baseURL = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';
  var relativeURLPath = isRelativeUrlPath(src);
  if (src.indexOf('//') === 0) {
    src = (!(0, _isServer.isServer)() ? window.location.protocol : 'https:') + src;
  }
  if (relativeURLPath) {
    src = relativeToAbsolutePath(baseURL, src);
  }
  return [src, (0, _isSvg.isSVG)(src)];
};
exports.getImgSRC = getImgSRC;
var relativeToAbsolutePath = function relativeToAbsolutePath(base, relative) {
  var root = relative[0] === '/';
  var resultBaseURL = getBaseURL(root, base);
  var stack = resultBaseURL.split('/');
  var parts = relative.split('/');
  if (stack.length > 1) {
    stack.pop(); // remove current file name (or empty string)
    // (omit if 'base' is the current folder without trailing slash)
  }

  if (root) {
    parts.shift();
  }
  for (var i = 0; i < parts.length; i++) {
    if (parts[i] === '.') continue;
    if (parts[i] === '..') stack.pop();else stack.push(parts[i]);
  }
  return stack.join('/');
};
var getBaseURL = function getBaseURL(root, base) {
  if (root) {
    return (base ? extractBaseURLFromString(base) : !(0, _isServer.isServer)() ? window.location.origin : '') + '/';
  } else {
    return base ? base : !(0, _isServer.isServer)() ? document.baseURI : 'http://localhost:3000/';
  }
};
var extractBaseURLFromString = function extractBaseURLFromString() {
  var path = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
  var pathArray = path.split('/');
  var protocol = pathArray[0];
  var host = pathArray[2];
  if (protocol && host) {
    return protocol + '//' + host;
  }
  return path;
};
var isRelativeUrlPath = function isRelativeUrlPath(src) {
  if (!src) return false;
  if (src.indexOf('//') === 0) {
    src = (!(0, _isServer.isServer)() ? window.location.protocol : 'https:') + src;
  }
  return src.indexOf('http://') !== 0 && src.indexOf('https://') !== 0 && src.indexOf('//') !== 0;
};