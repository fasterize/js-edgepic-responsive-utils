"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.picture = exports.img = exports.canvas = void 0;
var picture = function picture(_ref) {
  var preserveSize = _ref.preserveSize,
    imgNodeWidth = _ref.imgNodeWidth,
    imgNodeHeight = _ref.imgNodeHeight,
    ratio = _ref.ratio;
  return {
    width: preserveSize && imgNodeWidth ? imgNodeWidth : '100%',
    height: preserveSize && imgNodeHeight ? imgNodeHeight : 'auto',
    paddingBottom: preserveSize ? 'none' : 100 / ratio + '%',
    position: 'relative'
  };
};
exports.picture = picture;
var canvas = function canvas(_ref2) {
  var loaded = _ref2.loaded;
  return {
    width: '100%',
    height: '100%',
    position: 'absolute',
    top: '0',
    bottom: '0',
    left: '0',
    right: '0',
    opacity: loaded ? '0' : '1',
    zIndex: '1',
    transition: 'opacity 400ms ease 0ms'
  };
};
exports.canvas = canvas;
var img = function img() {
  return {
    display: 'block',
    width: '100%',
    position: 'absolute',
    opacity: 1,
    top: 0,
    left: 0
  };
};
exports.img = img;