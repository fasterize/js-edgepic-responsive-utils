"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.blurhash = exports.blurHashImgStyes = exports.backgroundStyles = exports.CONSTANTS = void 0;
Object.defineProperty(exports, "convertToPX", {
  enumerable: true,
  get: function get() {
    return _convertToPx.convertToPX;
  }
});
Object.defineProperty(exports, "determineContainerProps", {
  enumerable: true,
  get: function get() {
    return _determineContainerProps.determineContainerProps;
  }
});
Object.defineProperty(exports, "generateAlt", {
  enumerable: true,
  get: function get() {
    return _generateAlt.generateAlt;
  }
});
Object.defineProperty(exports, "generateURL", {
  enumerable: true,
  get: function get() {
    return _generateUrl.generateURL;
  }
});
Object.defineProperty(exports, "getBreakpoint", {
  enumerable: true,
  get: function get() {
    return _getBreakpoint.getBreakpoint;
  }
});
Object.defineProperty(exports, "getHeight", {
  enumerable: true,
  get: function get() {
    return _getHeight.getHeight;
  }
});
Object.defineProperty(exports, "getImgSRC", {
  enumerable: true,
  get: function get() {
    return _getImgSrc.getImgSRC;
  }
});
Object.defineProperty(exports, "getParamsFromURL", {
  enumerable: true,
  get: function get() {
    return _getParamsFromUrl.getParamsFromURL;
  }
});
Object.defineProperty(exports, "getParentContainerSize", {
  enumerable: true,
  get: function get() {
    return _getParentContainerSize.getParentContainerSize;
  }
});
Object.defineProperty(exports, "getPreviewSRC", {
  enumerable: true,
  get: function get() {
    return _getPreviewSrc.getPreviewSRC;
  }
});
Object.defineProperty(exports, "getRatio", {
  enumerable: true,
  get: function get() {
    return _getRatio.getRatio;
  }
});
Object.defineProperty(exports, "getSizeLimit", {
  enumerable: true,
  get: function get() {
    return _getSizeLimit.getSizeLimit;
  }
});
Object.defineProperty(exports, "getWidth", {
  enumerable: true,
  get: function get() {
    return _getWidth.getWidth;
  }
});
exports.imgStyles = void 0;
Object.defineProperty(exports, "isCrop", {
  enumerable: true,
  get: function get() {
    return _isCrop.isCrop;
  }
});
Object.defineProperty(exports, "isLowQualityPreview", {
  enumerable: true,
  get: function get() {
    return _isLowQualityPreview.isLowQualityPreview;
  }
});
Object.defineProperty(exports, "isSVG", {
  enumerable: true,
  get: function get() {
    return _isSvg.isSVG;
  }
});
Object.defineProperty(exports, "isServer", {
  enumerable: true,
  get: function get() {
    return _isServer.isServer;
  }
});
Object.defineProperty(exports, "isSupportedInBrowser", {
  enumerable: true,
  get: function get() {
    return _isSupportedInBrowser.isSupportedInBrowser;
  }
});
Object.defineProperty(exports, "processParams", {
  enumerable: true,
  get: function get() {
    return _processParams.processParams;
  }
});
Object.defineProperty(exports, "processReactNode", {
  enumerable: true,
  get: function get() {
    return _processReactNode.processReactNode;
  }
});
var _isServer = require("./utils/is-server");
var _getImgSrc = require("./utils/get-img-src");
var _getBreakpoint = require("./utils/get-breakpoint");
var _getPreviewSrc = require("./utils/get-preview-src");
var _getSizeLimit = require("./utils/get-size-limit");
var _getParentContainerSize = require("./utils/get-parent-container-size");
var _getWidth = require("./utils/get-width");
var _getHeight = require("./utils/get-height");
var _getRatio = require("./utils/get-ratio");
var _getParamsFromUrl = require("./utils/get-params-from-url");
var _isSvg = require("./utils/is-svg");
var _isCrop = require("./utils/is-crop");
var _isLowQualityPreview = require("./utils/is-low-quality-preview");
var _isSupportedInBrowser = require("./utils/is-supported-in-browser");
var _generateUrl = require("./utils/generate-url");
var _processReactNode = require("./utils/process-react-node");
var _processParams = require("./utils/process-params");
var _convertToPx = require("./utils/convert-to-px");
var _determineContainerProps = require("./utils/determine-container-props");
var _generateAlt = require("./utils/generate-alt");
var _imgStyles = _interopRequireWildcard(require("./utils/img.styles"));
exports.imgStyles = _imgStyles;
var _blurHashImgStyes = _interopRequireWildcard(require("./utils/blur-hash-img.styles"));
exports.blurHashImgStyes = _blurHashImgStyes;
var _backgroundStyles = _interopRequireWildcard(require("./utils/background.styles"));
exports.backgroundStyles = _backgroundStyles;
var _blurhash = _interopRequireWildcard(require("./libs/blur-hash"));
exports.blurhash = _blurhash;
var _CONSTANTS = _interopRequireWildcard(require("./constants"));
exports.CONSTANTS = _CONSTANTS;
function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }
function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }