"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DEVICE_PIXEL_RATIO_LIST = void 0;
var DEVICE_PIXEL_RATIO_LIST = [1, 1.5, 2];
exports.DEVICE_PIXEL_RATIO_LIST = DEVICE_PIXEL_RATIO_LIST;