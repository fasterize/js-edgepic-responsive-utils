"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.encode83 = exports.decode83 = void 0;
var digitCharacters = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "#", "$", "%", "*", "+", ",", "-", ".", ":", ";", "=", "?", "@", "[", "]", "^", "_", "{", "|", "}", "~"];
var decode83 = function decode83(str) {
  var value = 0;
  for (var i = 0; i < str.length; i++) {
    var c = str[i];
    var digit = digitCharacters.indexOf(c);
    value = value * 83 + digit;
  }
  return value;
};
exports.decode83 = decode83;
var encode83 = function encode83(n, length) {
  var result = "";
  for (var i = 1; i <= length; i++) {
    var digit = Math.floor(n) / Math.pow(83, length - i) % 83;
    result += digitCharacters[Math.floor(digit)];
  }
  return result;
};
exports.encode83 = encode83;