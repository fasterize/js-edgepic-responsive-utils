"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.isBlurhashValid = exports.default = void 0;
var _base = require("./base83");
var _utils = require("./utils");
var _error = require("./error");
/**
 * Returns an error message if invalid or undefined if valid
 * @param blurhash
 */
var validateBlurhash = function validateBlurhash(blurhash) {
  if (!blurhash || blurhash.length < 6) {
    throw new _error.ValidationError("The blurhash string must be at least 6 characters");
  }
  var sizeFlag = (0, _base.decode83)(blurhash[0]);
  var numY = Math.floor(sizeFlag / 9) + 1;
  var numX = sizeFlag % 9 + 1;
  if (blurhash.length !== 4 + 2 * numX * numY) {
    throw new _error.ValidationError("blurhash length mismatch: length is ".concat(blurhash.length, " but it should be ").concat(4 + 2 * numX * numY));
  }
};
var isBlurhashValid = function isBlurhashValid(blurhash) {
  try {
    validateBlurhash(blurhash);
  } catch (error) {
    return {
      result: false,
      errorReason: error.message
    };
  }
  return {
    result: true
  };
};
exports.isBlurhashValid = isBlurhashValid;
var decodeDC = function decodeDC(value) {
  var intR = value >> 16;
  var intG = value >> 8 & 255;
  var intB = value & 255;
  return [(0, _utils.sRGBToLinear)(intR), (0, _utils.sRGBToLinear)(intG), (0, _utils.sRGBToLinear)(intB)];
};
var decodeAC = function decodeAC(value, maximumValue) {
  var quantR = Math.floor(value / (19 * 19));
  var quantG = Math.floor(value / 19) % 19;
  var quantB = value % 19;
  var rgb = [(0, _utils.signPow)((quantR - 9) / 9, 2.0) * maximumValue, (0, _utils.signPow)((quantG - 9) / 9, 2.0) * maximumValue, (0, _utils.signPow)((quantB - 9) / 9, 2.0) * maximumValue];
  return rgb;
};
var decode = function decode(blurhash, width, height, punch) {
  validateBlurhash(blurhash);
  punch = punch | 1;
  var sizeFlag = (0, _base.decode83)(blurhash[0]);
  var numY = Math.floor(sizeFlag / 9) + 1;
  var numX = sizeFlag % 9 + 1;
  var quantisedMaximumValue = (0, _base.decode83)(blurhash[1]);
  var maximumValue = (quantisedMaximumValue + 1) / 166;
  var colors = new Array(numX * numY);
  for (var i = 0; i < colors.length; i++) {
    if (i === 0) {
      var value = (0, _base.decode83)(blurhash.substring(2, 6));
      colors[i] = decodeDC(value);
    } else {
      var _value = (0, _base.decode83)(blurhash.substring(4 + i * 2, 6 + i * 2));
      colors[i] = decodeAC(_value, maximumValue * punch);
    }
  }
  var bytesPerRow = width * 4;
  var pixels = new Uint8ClampedArray(bytesPerRow * height);
  for (var y = 0; y < height; y++) {
    for (var x = 0; x < width; x++) {
      var r = 0;
      var g = 0;
      var b = 0;
      for (var j = 0; j < numY; j++) {
        for (var _i = 0; _i < numX; _i++) {
          var basis = Math.cos(Math.PI * x * _i / width) * Math.cos(Math.PI * y * j / height);
          var color = colors[_i + j * numX];
          r += color[0] * basis;
          g += color[1] * basis;
          b += color[2] * basis;
        }
      }
      var intR = (0, _utils.linearTosRGB)(r);
      var intG = (0, _utils.linearTosRGB)(g);
      var intB = (0, _utils.linearTosRGB)(b);
      pixels[4 * x + 0 + y * bytesPerRow] = intR;
      pixels[4 * x + 1 + y * bytesPerRow] = intG;
      pixels[4 * x + 2 + y * bytesPerRow] = intB;
      pixels[4 * x + 3 + y * bytesPerRow] = 255; // alpha
    }
  }

  return pixels;
};
var _default = decode;
exports.default = _default;