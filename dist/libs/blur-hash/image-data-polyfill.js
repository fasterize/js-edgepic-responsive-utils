"use strict";

if (typeof window !== 'undefined') {
  if (window && window.CanvasPixelArray) {
    CanvasPixelArray.prototype.set = function (arr) {
      var l = this.length,
        i = 0;
      for (; i < l; i++) {
        this[i] = arr[i];
      }
    };
  }
}
(function () {
  if (typeof window !== 'undefined') {
    try {
      new window.ImageData(new Uint8ClampedArray([0, 0, 0, 0]), 1, 1);
    } catch (e) {
      var ImageDataPolyfill = function ImageDataPolyfill() {
        var args = Array.prototype.slice.call(arguments),
          data;
        if (args.length < 2) {
          throw new TypeError("\n          Failed to construct 'ImageData': 2 arguments required, but only ".concat(args.length, " present.\n        "));
        }
        if (args.length > 2) {
          data = args.shift();
          if (!(data instanceof Uint8ClampedArray)) {
            throw new TypeError("\n            Failed to construct 'ImageData': parameter 1 is not of type 'Uint8ClampedArray'\n          ");
          }
          if (data.length !== 4 * args[0] * args[1]) {
            throw new Error("\n            Failed to construct 'ImageData': The input data byte length is not a multiple of (4 * width * height)\n          ");
          }
        }
        var width = args[0],
          height = args[1],
          canvas = document.createElement('canvas'),
          ctx = canvas.getContext('2d'),
          imageData = ctx.createImageData(width, height);
        if (data) imageData.data.set(data);
        return imageData;
      };
      ;
      window.ImageData = ImageDataPolyfill;
    }
  }
})();