export const getParentContainerSize = (img, type = 'width') => {
    let parentNode = img;
    let size = 0;
    let maxCount = 0;

    // console.log('------------START-----------')
    do {
        parentNode = parentNode.parentNode;
        size = type === 'height' ? parentNode.clientHeight : parentNode.clientWidth;
        if (size) {
            size = typeof parentNode.getBoundingClientRect === 'function' ? parentNode.getBoundingClientRect()[type] : window.innerWidth;
        }
        // console.log('----------', maxCount)
        // console.log('img.NodeName',img.nodeName, img.alt )
        // console.log('size',size )
        // console.log('arguments', arguments)
        // console.log('maxCount',maxCount )
        // console.log('parentNode',parentNode )
        // console.log('parentNode.nodeName + nodeValue',parentNode.nodeName, parent.nodeValue )
        // console.log('bouding',typeof parentNode.getBoundingClientRect === 'function' ? parentNode.getBoundingClientRect() : null, window.innerWidth, type )
        // console.log('clientWidth',parentNode.clientWidth )
        // console.log('clientHeight',parentNode.clientHeight )
        // console.log('----------', maxCount)
        maxCount = maxCount + 1;

    } while (parentNode.parentNode && !size && maxCount < 5);

    // console.log('while size', size);

    const leftPadding = (size && parentNode && parentNode.nodeType === 1) ? parseInt(window.getComputedStyle(parentNode).paddingLeft) : 0;
    const rightPadding = (size && parentNode && parentNode.nodeType === 1) ? parseInt(window.getComputedStyle(parentNode).paddingRight) : 0;

    if (!size) {
        size = window.innerWidth;
    }

    // console.log('padding', leftPadding, rightPadding);
    // console.log('final size', size);
    // console.log('------------END-----------');

    return size + (size ? (-leftPadding - rightPadding) : 0);
};
